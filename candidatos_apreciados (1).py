import csv
import expresiones_regulares
import json
class tp2(object):
    def analizar_twit(self,archivo_normalizado):
        lista= json.load(open(archivo_normalizado, "r"))
        score=0
        cuenta=0
        twits=0
        file=open("STOP_WORDS_normalizado", 'r')
        for x in lista:
            for palabra in x[0].split():
                if len(palabra)<3 or palabra in file:
                    continue
                elif palabra in self.dicc_media_agrado:
                    score= score +  int(self.dicc_media_agrado[palabra])
                    cuenta = cuenta + 1
            twit+=1        
        file.close()
        self.tupla=(score/cuenta,twit)
        return tupla     
    def convertir(self):
        file_dic= open("meanAndStdev_normalizado.csv", 'r')
        r=csv.reader(file_dic,delimiter=';')
        linea=self.leer_registro(r)
        self.dicc_media_agrado={}
        while linea:
            y=linea[0] 
            while linea and y == linea[0]:
                self.dicc_media_agrado[linea[0]]=linea[1]
                linea=self.leer_registro(r)
        ##return self.dicc_media_agrado
        file_dic.close()
    def leer_registro(self,r):
        try:
            l=next(r)
            return l
        except StopIteration:
            return None
    def ranking_candidatos(self):
        
