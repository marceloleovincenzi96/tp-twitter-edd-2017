import re  
def procesar_twit_minuscula(twit):
    linea = re.sub(r'(\w+)',lambda x: pasador(x),twit)
    return linea
      
def pasador(grupo):
    groups1 =grupo.groups()
    return groups1[0].lower()

def salto_linea(twit):
    linea = re.sub(r'(\\n)',' ',twit)
    return linea

def comillas1(twit):
    linea = re.sub(r'(\\u201c)','“',twit)
    return linea

def comillas2(twit):
    linea = re.sub(r'(\\u201d)','”',twit)
    return linea

def continua(twit):
    linea = re.sub(r'(\\u2026)','',twit)
    return linea

def direccion_web_correo(twit):
    linea = re.sub(r'(www\.[\w.!#$%&*+/=?^_{|}~-]+\.\w+\.?\w*)|(https://t.co/.*\s)|([\w.!#$%&*+/=?^_{|}~-]+@[\w_-]+\.\w+\.?\w*)','URL',twit)
    return linea

def usuario(twit):
    linea = re.sub(r'(@[\w.!#$%&*+/=?^_{|}~-]+)','USER',twit)
    return linea

def reemplezar_vocales_a(stop_word):
    linea = re.sub(r'(á|Ã¡|\\xe1|\\xe0|\\xc1|\\xc0)','a',stop_word) 
    return linea

def reemplezar_vocales_e(stop_word):
    linea = re.sub(r'(é|Ã©|\\xe9\|\xe8|\\xc9|\\xc8)','e',stop_word) 
    return linea

def reemplezar_vocales_i(stop_word):
    linea = re.sub(r'(í|Ã­|\\xed|\\xec|\\xcc|\\xcd)','i',stop_word) 
    return linea

def reemplezar_vocales_o(stop_word):
    linea = re.sub(r'(ó|Ã³|\\xf3|\\xf2|\\xd2|\\xd3)','o',stop_word) 
    return linea

def reemplezar_vocales_u(stop_word):
    linea = re.sub(r'(ú|Ãº|\\xfa\|\xf9|\\xd9|\\xda)','u',stop_word) 
    return linea

def reemplezar_ñ(stop_word):
    linea = re.sub(r'Ã±|\\xf1','ñ',stop_word) 
    return linea

def eliminar_guion(stop_word):
    linea = re.sub(r'_[A-Z]','',stop_word) 
    return linea

def reemplazar_punto(stop_word):
    linea = re.sub(r'(\d)\.(\d)',r'\1'+r'\2',stop_word) 
    return linea

def emoji(stop_word):
    linea = re.sub(r'[\\]U0001f\w{3}','EMOJI',stop_word) 
    return linea

def eliminar_usuario(stop_word):
    linea = re.sub(r"[\w\d.!#$%&*+/=?^_{|}~-]+:\s('rt\s)?[\w\d.!@#$%&*+/=?^_{|}~-]*:?",'',stop_word) 
    return linea
