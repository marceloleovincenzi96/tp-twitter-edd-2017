import csv
import expresiones_regulares
import json
class normalizadora(object):
    def dicc_nor(self):
        file_dic= open("meanAndStdev.csv", 'r')
        salida = open("meanAndStdev_normalizado.csv", "w", newline="")
        r=csv.reader(file_dic,delimiter=';')
        linea=self.leer_registro(r)
        salida_read=csv.writer(salida,delimiter=";")
        while linea:
            y=linea[0] 
            while linea and y == linea[0]:
                valor1=expresiones_regulares.reemplezar_vocales_a(linea[0])
                valor2=expresiones_regulares.reemplezar_vocales_e(valor1)
                valor3=expresiones_regulares.reemplezar_vocales_i(valor2)
                valor4=expresiones_regulares.reemplezar_vocales_o(valor3)
                valor5=expresiones_regulares.reemplezar_vocales_u(valor4)
                valor6=expresiones_regulares.reemplezar_ñ(valor5)
                valor7=expresiones_regulares.eliminar_guion(valor6)
                valor8=expresiones_regulares.reemplazar_punto(linea[1])
                salida_read.writerow((valor7, valor8, linea [2], linea [3], linea [4], linea [5],linea [6]))
                linea=self.leer_registro(r)
        file_dic.close()
        salida.close()
        
    def leer_registro(self,r):
        try:
            return next(r)
        except StopIteration:
            return None
    def stop_nor(self):
        file_stop= open("STOP_WORDS", 'r')
        salida_stop = open("STOP_WORDS_normalizado", "w", newline="")
        for x in file_stop:
            valor1=expresiones_regulares.reemplezar_vocales_a(x)
            valor2=expresiones_regulares.reemplezar_vocales_e(valor1)
            valor3=expresiones_regulares.reemplezar_vocales_i(valor2)
            valor4=expresiones_regulares.reemplezar_vocales_o(valor3)
            valor5=expresiones_regulares.reemplezar_vocales_u(valor4)
            salida_stop.write(valor5) 
        salida_stop.write("EMOJI")    
        file_stop.close()
        salida_stop.close()
        
    def twit_nor(self,archivo_e,archivo_s):
        twits = json.load(open(archivo_e,"r"))
        lista1=[]
        for x in twits:
            valor=expresiones_regulares.procesar_twit_minuscula(x[0])
            valor0=expresiones_regulares.eliminar_usuario(valor)
            valor1=expresiones_regulares.reemplezar_vocales_a(valor0)
            valor2=expresiones_regulares.reemplezar_vocales_e(valor1)
            valor3=expresiones_regulares.reemplezar_vocales_i(valor2)
            valor4=expresiones_regulares.reemplezar_vocales_o(valor3)
            valor5=expresiones_regulares.reemplezar_vocales_u(valor4)
            valor6=expresiones_regulares.direccion_web_correo(valor5)
            valor7=expresiones_regulares.usuario(valor6)
            valor8=expresiones_regulares.continua(valor7)
            valor9=expresiones_regulares.salto_linea(valor8)
            valor10=expresiones_regulares.comillas1(valor9)
            valor11=expresiones_regulares.comillas2(valor10)
            valor12=expresiones_regulares.emoji(valor11)
            valor13=expresiones_regulares.reemplezar_ñ(valor12)
            lista1.append([valor13,x[1],x[2]])
        json.dump (twits_normalizados, open(archivo_s, "w"))
        
    def cargar_cfk1(self):
    
        lista_cfk1 = json.load(open("cfkargentina_nor.j", "r"))

        return lista_cfk1                
            
